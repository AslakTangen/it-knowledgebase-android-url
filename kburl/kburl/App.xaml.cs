﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace kburl
{
    public partial class App : Application
    {
        public App ()
        {
            MainPage = new kburl.MainPage();
        }

        protected override void OnStart ()
        {
            // Handle when your app starts
            Console.WriteLine("App OnStart");
        }

        protected override void OnSleep ()
        {
            // Handle when your app sleeps
            Console.WriteLine("App OnSleep");
        }

        protected override void OnResume ()
        {
            // Handle when your app resumes
            Console.WriteLine("App OnResume");
        }
    }
}
