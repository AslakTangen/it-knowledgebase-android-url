﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Xamarin.Forms;

namespace kburl
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
            Device.OpenUri(new Uri("https://laerdal.egain.cloud/system/templates/selfservice/laerdal/help/agent/locale/en-US/portal/404700000001000"));
            if (Device.RuntimePlatform == Device.Android)
            {
                DependencyService.Get<IAndroidMethods>().CloseApp();
            }
        }
	}
}
