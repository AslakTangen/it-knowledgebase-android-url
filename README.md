# KnowledgeBase Android URL

Simple Android App that launches the default browser with the URL to KnowledgeBase

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
Visual Studio 2017 with Xamarin tools installed
```

### Installing

A step by step series of examples that tell you have to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Visual Studio Tools for Xamarin](https://www.visualstudio.com/xamarin/)

## Versioning

We use tags in the BitBucket git repository for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Aslak Tangen** - *Initial work* - [BitBucket Profile](https://bitbucket.org/AslakTangen/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* [www.intertech.com](https://www.intertech.com/Blog/xamarin-tutorial-part-1-create-a-blank-app/) - For the tutorial
